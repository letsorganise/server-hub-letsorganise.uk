// Copyright (C) 2017 Harry Gill
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cookieParser from 'cookie';
import socketRedis from 'socket.io-redis';

import { decodeToken } from '../helpers/crypto';
// import store from './userStore';
import handleMessages from './handle-messages';

const adapter = socketRedis({ host: 'localhost', port: 6379 });

export default ios => {
  // setup IO server
  ios.adapter(adapter);
  ios.use(authorize);

  ios.on('connection', function(socket) {
    onConnectionSetup({ socket, ios });
    handleMessages({ socket, ios });
  });

  ios.on('disconnect', socket => {
    console.log('disconnected', socket);
    // store.removeUserById(socket.user.id);
    // ios.emit('setOnlineUsers', store.getAllUsers());
  });
};

const authorize = (socket, next) => {
  if (socket.request.headers.cookie) {
    const { session } = cookieParser.parse(socket.request.headers.cookie);
    if (session) {
      const { token } = JSON.parse(Buffer.from(session, 'base64').toString());
      socket.user = decodeToken(token);
    }
    return socket.user ? next() : next(new Error('Not Authorised'));
  }
  next(new Error('Not Authorised'));
};

const onConnectionSetup = ({ socket, ios }) => {
  // store.addUser(socket, ios).then(() => {
  //   ios.emit('setOnlineUsers', store.getAllUsers());
  //   // console.error(store.getAllUsers());
  // });
  socket.on('join_conversation', function(conversation) {
    if (socket.conversation) {
      socket.leave(socket.conversation);
    }
    socket.join(conversation);
    socket.conversation = conversation;
    console.error('join_conversation', socket.conversation);
  });

  socket.on('getOnlineUsers', function() {
    // ios.emit('setOnlineUsers', store.getAllUsers());
  });

  socket.on('disconnecting', reason => {
    // referesh page etc
    console.log('socket disconnected', socket.user.id, reason);
    // store.removeUserById(socket.user.id);
    // ios.emit('setOnlineUsers', store.getAllUsers());
  });

  ios.of('/').adapter.clients((err, clients) => {
    if (err) {
      console.error(err);
    }
    console.log(clients); // an array containing all connected socket ids
  });
};
