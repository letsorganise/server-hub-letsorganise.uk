// Copyright (C) 2017 Harry Gill
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { User } from '../models/';
const Users = new Map();

export default {
  addUser: (socket, ios) => {
    const user = Users.get(socket.user.id);
    if (user) {
      ios.of('/').adapter.remoteDisconnect(user.id, false, function(err) {
        if (err) {
          console.log('Error disconnecting remote User', user, err);
        }
        // success
      });
      user.socketId = socket.id;

      Users.set(user.id, user); // replace user with new id
      return Promise.resolve();
    } else {
      // socket.user.id = socket.id;
      return getUserfromDb(socket.user.id).then(user => {
        user.socketId = socket.id;
        Users.set(user.id, user);
        return true; // added a new user
      });
    }
  },
  getUserById: id => {
    return Users.get(id);
  },
  removeUserById: id => {
    Users.delete(id);
  },
  getAllUsers: () => {
    return Array.from(Users.values());
  }
};

const getUserfromDb = async id => {
  const user = User.findById(id).then(user => user.toJSON());
  return user;
};
