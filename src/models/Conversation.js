/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ConversationModel - generates a sequelize model for Conversations
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

import { join } from 'path';
export default (sequelize, DataTypes) => {
  const User = sequelize.import(join(__dirname, 'User.js'));

  const Conversation = sequelize.define(
    'Conversation',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        validate: {
          isUUID: 4
        }
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      createdBy: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: User,
          key: 'id'
        }
      },
      lastActive: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: true
        }
      }
    },
    {
      paranoid: true,
      indexes: [
        {
          fields: ['name'],
          unique: true
        }
      ]
    }
  );

  Conversation.associate = models => {
    Conversation.hasMany(models.Message);
    Conversation.belongsTo(models.User, {
      as: 'Creator',
      foreignKey: 'createdBy',
      targetKey: 'id',
      allowNull: false
    });
    Conversation.belongsToMany(models.User, {
      as: 'Participants',
      through: models.ConversationParticipants,
      foreignKey: 'ConversationId',
      otherKey: 'UserId'
    });
  };
  return Conversation;
};
