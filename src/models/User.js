/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { hashPwd, isValidUserId } from '../helpers/crypto';

/**
 * UserModel - generates a sequelize model for Users
 *
 * @param {object} sequelize sequelize
 * @param {object} DataTypes sequelize data types
 *
 * @returns {object} Sequelize Model
 * @see http://docs.sequelizejs.com/manual/tutorial/models-definition.html
 *
 */

export default (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
        validate: {
          validId(uId) {
            if (!isValidUserId(uId)) {
              throw new Error('@id must be 5 to 20 character long');
            }
          }
        }
      },
      createdBy: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          validId(uId) {
            if (!isValidUserId(uId)) {
              throw new Error('@id must be 5 to 20 character long');
            }
          }
        }
      },
      password: {
        type: DataTypes.STRING,
        allowNull: true
      },
      lastActive: {
        type: DataTypes.DATE,
        allowNull: true,
        validate: {
          isDate: true
        }
      },
      name: {
        type: DataTypes.VIRTUAL,
        get() {
          // 'this' allows you to access attributes of the instance
          const firstName = this.getDataValue('firstName');
          const lastName = this.getDataValue('lastName');
          const middleName = this.getDataValue('middleName');
          const initial = middleName ? middleName.charAt(0) : '';
          const fullName = (firstName || '') + ' ' + initial + ' ' + (lastName || '');
          return fullName.replace(/ +/g, ' ').trim(); // remove any extra spaces
        }
      },
      firstName: {
        type: DataTypes.STRING,
        defaultValue: 'Name',
        validate: {
          len: {
            args: [2, 65],
            msg: 'firstName must be between 2 to 65 character long'
          }
        }
      },
      middleName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [1, 30],
            msg: 'middleName must be between 1 to 30 character long'
          }
        }
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [2, 65],
            msg: 'lastName must be between 2 to 65 character long'
          }
        }
      },

      email: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Not a valid email'
          }
        }
      },

      phone: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [6, 20],
            msg: 'Not a valid phone'
          }
        }
      }
    },
    {
      paranoid: true,
      indexes: [
        {
          fields: ['id'],
          unique: true
        },
        {
          fields: ['email'],
          unique: true
        }
      ]
    }
  );

  User.afterValidate((user, options) => {
    // return if there is already a hash there
    if (!user.dataValues.password || user.dataValues.password.indexOf('$2a$') === 0) {
      return;
    }
    return hashPwd(user.password).then(hash => {
      user.password = hash;
    });
  });

  User.associate = function(models) {
    User.hasOne(models.UserSetting);
    User.belongsToMany(models.Group, {
      as: 'Groups',
      through: models.GroupUsers
    });
    User.belongsToMany(models.Conversation, {
      as: 'Conversations',
      through: models.ConversationParticipants,
      foreignKey: 'UserId',
      otherKey: 'ConversationId'
    });
  };

  User.prototype.toJSON = function() {
    const val = Object.assign({}, this.get());
    delete val.password;
    return val;
  };

  return User;
};
