/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
require('dotenv').config({ silent: true });

export default {
  NODE_ENV: process.env.NODE_ENV,
  FQDN: process.env.FQDN,
  port: process.env.PORT,
  socketPort: process.env.SOCKET_PORT || 2525,
  saltRounds: Number(process.env.SALT_ROUNDS) || 13,
  uploadPath: process.env.ABSOLUTE_UPLOADS_PATH || path.join(__dirname, '../../uploads'),
  pubKey: process.env.PUB_KEY_PATH,
  priKey: process.env.PRI_KEY_PATH,
  keys: [process.env.KEY0, process.env.KEY1, process.env.KEY2],
  emailKey: process.env.MAIL_KEY,
  emailDomain: process.env.MAIL_DOMAIN,
  emailSender: process.env.MAIL_SENDER
};
export const dbConfig = () => {
  if (process.env.CI === 'true') {
    return {
      database: process.env.POSTGRES_DB,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      host: process.env.POSTGRES_HOST,
      port: process.env.POSTGRES_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      pool: {
        max: 5,
        min: 1,
        idle: 10000
      },
      logging: false // (e) => console.error(e)
    };
  } else if (process.env.NODE_ENV === 'production') {
    return {
      database: process.env.DB_DATABASE,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      logging: false,
      pool: {
        max: 95,
        min: 5,
        idle: 10000
      }
    };
  } else if (process.env.NODE_ENV === 'testing') {
    return {
      database: process.env.DB_DATABASE,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: 'postgres',
      // timezone: '+01:00',
      logging: false,
      pool: {
        max: 95,
        min: 5,
        idle: 1000
      }
    };
  }

  return {
    database: process.env.RESET ? 'reset' : process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    // timezone: '+01:00',
    pool: {
      max: 5,
      min: 1,
      idle: 10000
    },
    logging: false
  };
};
