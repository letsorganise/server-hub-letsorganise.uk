/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { join } from 'path';
import fs from 'fs';
import rfs from 'rotating-file-stream';
import morgan from 'morgan';
import config from '../helpers/config';

morgan.token('user', function (req, res) {
  return req.user ? req.user.id : '@anon';
});

const logString =
  '[:date[clf]] :remote-addr :user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms - ":user-agent"';

const logDirectory = join(__dirname, '../', '../', 'logs');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const accessLogStream = rfs(generator, {
  size: '2M',
  interval: '1d', // rotate daily
  path: logDirectory,
  rotationTime: true
});

export default app => {
  app.use(morgan(logString, { stream: accessLogStream }));

  if (config.NODE_ENV === 'development') {
    app.use(morgan('dev'));
  }
};

function pad(num) {
  return (num > 9 ? '' : '0') + num;
}

function generator(time, index) {
  if (!time) return 'access.log';

  var month = time.getFullYear() + '' + pad(time.getMonth() + 1);
  var day = pad(time.getDate());
  var hour = pad(time.getHours());
  var minute = pad(time.getMinutes());

  return month + '/' + month + day + '-' + hour + minute + '-' + index + '-access.log';
}
