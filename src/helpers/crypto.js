/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// @flow
import bcrypt from 'bcrypt';
import config from './config';
import jwt from 'jsonwebtoken';
import uuid from 'uuid/v4';
import { readFileSync } from 'fs';
import { join } from 'path';
import commons from './common-passwords';
export const generateId = () => uuid();

/**
 * isValidUUID - Validates a UUIDV4
 *
 * @param {string} str UUIDV4 string
 *
 * @returns {boolean} returns true if input is a valid UUIDV4
 */
export const isValidUUID = str => {
  if (!str || typeof str !== 'string') {
    return false;
  }
  if (str.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i)) {
    return true;
  }
  return false;
};

/**
 * isValidUUID - Validates a id
 *
 * @param {string} str @id
 *
 * @returns {boolean} returns true if input is a valid id
 */
export const isValidUserId = str => {
  if (!str || typeof str !== 'string') {
    return false;
  }
  return /^\B[@＠]([a-z_]{4,20})$/.test(str);
};

export const isPwdStrong = str => {
  if (!str || typeof str !== 'string') {
    return false;
  }
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,72}$/.test(str) && !commons.includes(str);
};

/**
 * hashPwd - hash plain text password
 *
 * @param {!string} plain plaintext password
 *
 * @returns {string} empty string or hashed password
 */
export const hashPwd = async plain => {
  if (!plain) {
    return Promise.resolve('');
  }
  try {
    return await bcrypt.hash(plain, config.saltRounds).then(hashed => hashed);
  } catch (e) {
    console.error('Error Hashing Password', e.name, e.message);
    return Promise.resolve('');
  }
};

/**
 * checkPwd - Verify password with hash
 *
 * @param {!string} password Plaintext password from the user
 * @param {!string} hash     Hash from the database
 *
 * @returns {boolean} Returns true if match
 */
export const checkPwd = async (password, hash) => {
  if (!password || !hash) {
    return false;
  }
  try {
    return bcrypt.compare(password, hash).then(res => res);
  } catch (e) {
    console.error(e.name, e.message);
    return false;
  }
};

const priKey = readFileSync(join(__dirname, '../../', config.priKey));
const pubKey = readFileSync(join(__dirname, '../../', config.pubKey));

/**
 * createToken - Create token from the User Object
 *
 * @param {!object} user the user object
 *
 * @returns {strong} base64 encoded JWT token
 */
export const createToken = user => {
  const options = {
    expiresIn: '14d',
    issuer: 'letsorganise.uk',
    algorithm: 'RS384'
  };
  const { id, Groups } = user;
  try {
    return jwt.sign({ id, Groups }, priKey, options);
  } catch (e) {
    console.error(e);
    return false;
  }
};

/**
 * decodeToken - Verify if the token in valid
 *
 * @param {!string} token base64 JWT token
 *
 * @returns {object|boolean} Returns decoded token object or false
 */
export const decodeToken = token => {
  if (typeof token === 'string') {
    try {
      return jwt.verify(token, pubKey, { algorithms: ['RS384'] });
    } catch (e) {
      console.error(e.name, e.message);
      return false;
    }
  } else {
    return false;
  }
};
