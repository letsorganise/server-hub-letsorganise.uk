/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Group, User, GroupUsers } from '../../../models';
import handleErrors, { errorOnInvalidId } from '../utils/handleErrors';
import { isAdmin } from '../utils/authorisation';

const route = express.Router();
route.use(errorOnInvalidId);
route.post('/groups/:id/user/:userId', async (req, res, next) => {
  try {
    const { permission } = req.body;
    if (!permission || permission > 7 || permission < 4) {
      return next(handleErrors('Permission value must be between 4 and 7'));
    }
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }

    const group = await Group.findById(req.params.id);
    const user = await User.findById(req.params.userId);
    if (group && user) {
      await GroupUsers.create({ GroupId: group.id, UserId: user.id, permission });

      res.json({ message: `${user.id} added to the ${group.name} group` });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.delete('/groups/:id/user/:userId', async (req, res, next) => {
  try {
    const { id, userId } = req.params;
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }

    const group = await Group.findById(id);
    const user = await User.findById(userId);
    if (group && user) {
      // user.GroupUsers = { permission: permission };
      group.removeUser(user);
      res.json({
        message: `${user.name} removed from the ${group.name} group`
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
