/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import template from './template';
import { sendEmail } from './mailgun';
// import fs from 'fs';

// import { join } from 'path';

// const staticFolder = join(__dirname, '..', '..', '..', 'static');

const domain = process.env.FQDN + '/#/confirm/set-password';

export const emailSetPassword = (user, newUser, token, exp) => {
  const subject = "Welcome to Let's Organise";
  const html = template({
    title: subject,
    name: newUser.name,
    textAbove: `Welcome to Let's Organise App. Your account has been created, 
      please clik on the button below to activte. 
      You will be asked for a new password before you can login for the first time. 
      Link will expire on ${exp}.`,
    callToAction: 'Activate',
    url: `${domain}/${token}?id=${newUser.id}`,
    textBelow:
      'You can reply to this email to contact the Admin. If you believe this is an error, please delete this email immedeately.',
    signature: 'Regards <br/>' + user.name
  });
  // if (process.env.NODE_ENV === 'development') {
  //   console.error('Only in Dev mode');
  //   console.error(html);
  // }
  // fs.writeFileSync(join(staticFolder, newUser.name + 'email.html'), html, 'utf-8');
  return sendEmail(user.email, newUser.email, subject, html);
};

export const emailResetPassword = (user, newUser, token, exp) => {
  const subject = "Let's Organise, Password Reset";
  const html = template({
    title: subject,
    name: newUser.name,
    textAbove: `Your account password has been reset, 
      please clik on the button below to set your new password.
      Link will expire on <strong>${exp}</strong>.`,
    callToAction: 'Reset Password',
    url: `${domain}/${token}?id=${newUser.id}`,
    textBelow: 'You can reply to this email to contact the Admin.',
    signature: 'Regards <br/>' + user.name
  });
  if (process.env.NODE_ENV === 'development') {
    console.info(html);
  }
  // fs.writeFileSync(join(staticFolder, newUser.name + 'email.html'), html, 'utf-8');
  return sendEmail(user.email, newUser.email, subject, html);
};
