/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import config from '../../../helpers/config';
import mailgunJs from 'mailgun-js';

// This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
export const mailgun = mailgunJs({
  apiKey: config.emailKey,
  domain: config.emailDomain
});

// https://stackoverflow.com/questions/18371339/how-to-retrieve-name-from-email-address
export const fixSenderDomain = email => {
  if (!email) {
    return '';
  }
  const name = email.substring(0, email.lastIndexOf('@'));
  const domain = email.substring(email.lastIndexOf('@') + 1);
  if (domain === config.emailDomain) {
    return email;
  }
  return name + '@' + config.emailDomain;
};

export const sendEmail = (fromEmail, toEmail, subject, body) => {
  const message = {
    from: fixSenderDomain(fromEmail) || config.emailSender,
    to: toEmail, // for testig only,
    subject,
    'h:Reply-To': fromEmail || config.emailSender,
    text: body,
    html: body
  };

  return new Promise((resolve, reject) => {
    mailgun.messages().send(message, function(err, info) {
      if (err) {
        console.error('Sending Email Error: ' + err);
        reject(err);
      } else {
        console.log('\r\rMailgun Response: ' + JSON.stringify(info));
        resolve(info);
      }
    });
  });
};
