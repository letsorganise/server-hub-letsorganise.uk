/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
import fs from 'fs';
import multer from 'multer';

import config from '../../../helpers/config';

export default (fieldname = 'file') => {
  const uploading = multer({
    storage: multer.diskStorage({
      destination: path.join(config.uploadPath, fieldname),
      filename: function (req, file, cb) {
        const name = `${req.params.id}_${Date()}_${file.originalname}`;
        cb(null, name);
      }
    }),
    fileFilter: (req, file, cb) => {
      cb(null, whiteListedTypes.includes(file.mimetype));

      // You can always pass an error if something goes wrong:
      // cb(new Error("I don't have a clue!"));
    }
  });

  return uploading.single(fieldname);
};

const whiteListedTypes = [
  'image/png',
  'image/gif',
  'image/jpeg',
  'image/svg+xml',
  'application/pdf',
  'application/msword', // doc
  'application/vnd.oasis.opendocument.text', // odt
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // docx
];

export const deleteFile = file => {
  if (file && file.path) {
    fs.unlink(file.path);
  }
};
