/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// import * as db from '../../../models';

// const permissionObject = {
//   value: 0,
//   canRead() {
//     return this.value >= 3;
//   },
//   canUpdate() {
//     return this.value >= 5;
//   },
//   // canCreate() {
//   // return this.value >= 6;
//   // },
//   canDelete() {
//     return this.value >= 7;
//   }
// };

export const isRoot = user => {
  return user.id === '@root';
};

export const isAdmin = user => {
  let granted = isRoot(user);
  user.Groups.forEach(g => {
    if (g.type === 'ADMIN') {
      granted = true;
    }
  });
  return granted;
};

// export const userPermissions = (user: any) => {
// const permission = Object.assign({}, permissionObject);
// if (isAdmin(user)) {
// permission.value = 8;
// return Promise.resolve(permission);
// }

// if user is member of the type 'USER' groups
// assign the hightest value
// user.Groups.map(group => {
// if (group.type === 'USER') {
// if (group.permission > permission.value) {
// permission.value = group.permission;
// }
// }
// });
// return Promise.resolve(permission);
// };

// export const groupPermissions = (user: any) => {
// const permission = Object.assign({}, permissionObject);
// if (isAdmin(user)) {
// permission.value = 8;
// return Promise.resolve(permission);
// }
// user.Groups.map(g => {
// if (g.type === 'GROUP') {
// if (g.permission > permission.value) {
// permission.value = g.permission;
// }
// }
// });
// return Promise.resolve(permission);
// };
