/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import redis from 'redis';
import bluebird from 'bluebird';
import uuid from 'uuid';
import addSeconds from 'date-fns/add_seconds';
import format from 'date-fns/format';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient();

const ONE_DAY = 60 * 60 * 24;

client.on('error', err => {
  console.error('Redis Client Error ' + err);
});

export const setPasswordToken = async userId => {
  const token = uuid();
  const now = new Date();
  await client.SETEXAsync(userId + ':resetToken', ONE_DAY, token);

  return {
    token,
    exp: format(addSeconds(now, ONE_DAY), 'DD/MM/YYYY hh:mm A')
  };
};

export const getPasswordToken = userId => {
  return client.getAsync(userId + ':resetToken');
};

export const deletePasswordToken = userId => {
  return client.delAsync(userId + ':resetToken');
};
export default client;
