/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { User } from '../../../models';
import handleErrors from '../utils/handleErrors';
import { getPasswordToken, deletePasswordToken } from '../utils/redisClient';
import { isPwdStrong } from '../../../helpers/crypto';

const route = Router();

route.get('/set-password/:id', async (req, res, next) => {
  try {
    const token = await getPasswordToken(req.params.id);
    if (!token) {
      return next(handleErrors(404));
    }
    res.json({});
  } catch (e) {
    next(handleErrors('Token is expired'));
  }
});

route.post('/set-password/:id', async (req, res, next) => {
  try {
    const token = await getPasswordToken(req.params.id);

    if (!token || token !== req.body.token) {
      return next(handleErrors(404));
    }
    const user = await User.findById(req.params.id);
    if (!user) {
      return next(handleErrors(404));
    }
    if (!isPwdStrong(req.body.password)) {
      return next(handleErrors('Password is too common, please try again.'));
    }
    return user.update({ password: req.body.password }).then(() => {
      deletePasswordToken(req.params.id);
      res.json({});
    });
  } catch (e) {
    next(handleErrors('Invalid Token'));
  }
});

export default route;
