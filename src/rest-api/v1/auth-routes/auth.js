/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';

import { createToken, checkPwd, generateId } from '../../../helpers/crypto';
import { User, Group, UserSetting, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import mergeUser from '../utils/mergeUser';
import { authValidate } from './validations';
const route = Router();

route.post('/auth', authValidate, async (req, res, next) => {
  const { id, password } = req.body;
  try {
    const user = await User.findOne({
      where: { id: { [Op.eq]: id } },
      include: [{ model: Group, as: 'Groups' }, { model: UserSetting }]
    }).catch(e => {
      console.error(e);
    });

    if (!user || !await checkPwd(password, user.password)) {
      req.session = null;
      return next(handleErrors('Wrong id and/or Password'));
    }

    const userToSend = mergeUser(user);

    const token = createToken(userToSend);
    if (!token) {
      return next(handleErrors(500));
    }
    req.session.token = token;

    if (req.body.rememberMe) {
      const WeekInMilliseconds = 604800000;
      const expireDate = new Date(new Date().getTime() + WeekInMilliseconds * 2); // two weeks
      req.sessionOptions.expires = expireDate;
    }
    req.session.id = generateId();
    return res.json({ user: userToSend, token: token, admin: getAdminVal(user) });
  } catch (e) {
    req.session = null;
    return next(handleErrors(e));
  }
});

route.delete('/auth', (req, res) => {
  req.session = null;
  res.json({ msg: 'LoggedOut' });
});

route.get('/auth', async (req, res, next) => {
  try {
    if (req.user) {
      const user = await User.findOne({
        where: { id: { [Op.eq]: req.user.id } },
        include: [{ model: Group, as: 'Groups' }]
      });

      if (!user) {
        req.session = null;
        return next(handleErrors(401));
      } else {
        res.json({ user: mergeUser(user), admin: getAdminVal(user) });
      }
    } else {
      req.session = null;
      next(handleErrors(401));
    }
  } catch (e) {
    return next(e);
  }
});

export default route;

const getAdminVal = user => {
  const MaxAdminValue = 8;
  const MinAdminValue = 0;
  const AdminGroupTypeName = 'ADMIN';

  let adminVal = user.id === '@root' ? MaxAdminValue : MinAdminValue;
  user.Groups.forEach(g => {
    if (g.type === AdminGroupTypeName && adminVal <= g.GroupUsers.permission) {
      adminVal = g.GroupUsers.permission;
    }
  });
  return adminVal;
};
