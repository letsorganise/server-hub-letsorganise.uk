/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { isAdmin } from '../utils/authorisation';
import { User, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import configFromQuery from '../utils/config-from-query';


const route = Router();

route.delete('/users/:id', async (req, res, next) => {
  try {
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }
    const user = await User.findById(req.params.id);
    if (user) {
      user.destroy({ force: process.env.NODE_ENV !== 'production' });
      res.json({ msg: 'deleted' });
    } else {
      return next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/users/:id', async (req, res, next) => {
  try {
    const authorised = req.user.id === req.params.id || isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }
    const config = configFromQuery(req.query);
    // config.include = config.include || [];
    // config.include.push({ model: Group, as: 'Groups' }, { model: Worker, as: 'Worker' });
    // config.include = R.uniq(config.include);

    config.where = { id: { [Op.eq]: req.params.id } };
    const user = await User.findOne(config);
    if (user) {
      res.json((user));
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/users/:id/check', async (req, res, next) => {
  try {
    if (!isAdmin(req.user)) {
      return next(handleErrors(401));
    }
    console.error('test');
    const user = await User.findById(req.params.id);
    if (user) {
      return next(handleErrors(409));
    } else {
      res.json({});
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.put('/users/:id', async (req, res, next) => {
  try {
    const data = req.body;
    const authorised = req.user.id === req.params.id || isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }
    const user = await User.findById(data.id);

    if (user) {
      if (data.password) {
        delete data.password;
      }
      user.update(data).then(updated => {
        // toJSON is required to make sure password is not sent to the client
        res.json(updated.toJSON());
      });
    } else {
      next(handleErrors(404));
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
