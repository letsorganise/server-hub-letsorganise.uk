/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Router } from 'express';
import { isAdmin } from '../utils/authorisation';
import { User, sequelize, UserSetting, Op } from '../../../models';
import handleErrors from '../utils/handleErrors';
import configFromQuery from '../utils/config-from-query';


import { setPasswordToken } from '../utils/redisClient';
import { emailSetPassword } from '../utils/pwd-emails';

const route = Router();

route.post('/users', async (req, res, next) => {
  try {
    const authorised = isAdmin(req.user);
    if (!authorised) {
      return next(handleErrors(401));
    }
    const potUser = Object.assign({}, req.body, { createdBy: req.user.id });
    if (potUser.password) { // don't save password user will be sent an email
      delete potUser.password;
    }

    const settings = await sequelize.transaction(t => {
      return User.create(potUser, { transaction: t }).then(newUser => {
        return UserSetting.create({ UserId: newUser.id, active: true }, { transaction: t });
      });
    });
    if (settings) {
      // console.error(settings);

      User.findOne({
        where: { id: { [Op.eq]: potUser.id } }
      }).then(async user => {
        const result = await setPasswordToken(user.id);
        // const mergedUser = meregeUser(user);
        return emailSetPassword(req.user, user, result.token, result.exp).then(() => {
          return res.json(user);
        });
      });
    }
  } catch (e) {
    next(handleErrors(e));
  }
});

route.get('/users', async (req, res, next) => {
  try {
    const config = configFromQuery(req.query);
    config.where = { id: req.user.id };
    const authorised = isAdmin(req.user);
    if (authorised) {
      delete config.where;
    }

    // config.include = config.include || [];
    // config.include.push({ model: Worker, as: 'Worker' });
    // config.include = R.uniq(config.include);


    User.findAll(config).then(users => {
      res.json(users);
    });
  } catch (e) {
    next(handleErrors(e));
  }
});

export default route;
