/**
 * Copyright (C) 2017-Present Let's Organise Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import express from 'express';
import { Conversation, User, ConversationParticipants } from '../../../models';
import handleErrors from '../utils/handleErrors';
// import { isAdmin } from '../utils/authorisation';
// import configFromQuery from '../utils/config-from-query';

const route = express.Router();

route.get('/conversations', async (req, res, next) => {
  try {
    const CurrentUser = await User.findById(req.user.id);
    const conversations = await CurrentUser.getConversations();

    res.json(conversations);
  } catch (e) {
    console.error(e);
    next(handleErrors(e));
  }
});

route.post('/conversations', async (req, res, next) => {
  try {
    const potConversation = req.body;
    potConversation.createdBy = req.user.id;
    const Convo = await Conversation.create(potConversation);
    await ConversationParticipants.create({ ConversationId: Convo.id, UserId: req.user.id });
    return res.json(Convo);
  } catch (e) {
    return next(handleErrors(e));
  }
});

export default route;
