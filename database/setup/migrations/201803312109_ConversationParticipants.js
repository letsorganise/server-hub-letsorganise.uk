module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('ConversationParticipants', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        UserId: {
          type: DataTypes.STRING,
          allowNull: false
        },
        ConversationId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('ConversationParticipants', ['UserId', 'ConversationId'], {
          type: 'unique',
          name: 'ConversationParticipants_UserId_ConversationId_key'
        });
      })
      .then(() => {
        return query.addConstraint('ConversationParticipants', ['ConversationId'], {
          type: 'FOREIGN KEY',
          name: 'ConversationParticipants_ConversationId_fkey',
          references: {
            table: 'Conversations',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('ConversationParticipants', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'ConversationParticipants_UserId_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('ConversationParticipants');
  }
};
