import groups from '../groups';
module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Groups', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        name: {
          type: DataTypes.STRING,
          unique: true
        },
        type: {
          type: DataTypes.STRING,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: DataTypes.NOW
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('Groups', ['type'], {
          type: 'FOREIGN KEY',
          name: 'Groups_type_fkey',
          references: {
            table: 'GroupTypes',
            field: 'name'
          }
        });
      })
      .then(() => {
        return query.bulkInsert('Groups', groups);
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Groups');
  }
};
