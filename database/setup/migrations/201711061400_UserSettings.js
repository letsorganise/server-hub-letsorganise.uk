module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('UserSettings', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        active: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
        },
        UserId: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        }
      })
      .then(() => {
        return query.addConstraint('UserSettings', ['UserId'], {
          type: 'FOREIGN KEY',
          name: 'UserSettings_UserId_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('UserSettings');
  }
};
