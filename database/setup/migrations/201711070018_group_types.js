module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('GroupTypes', {
        name: {
          type: DataTypes.STRING,
          unique: true,
          primaryKey: true
        }
      })
      .then(() => {
        return query.bulkInsert('GroupTypes', [{ name: 'ADMIN' }]);
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('GroupTypes');
  }
};
