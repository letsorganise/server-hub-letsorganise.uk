module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Conversations', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        name: {
          type: DataTypes.STRING,
          allowNull: true
        },
        createdBy: {
          type: DataTypes.STRING,
          allowNull: false
        },
        lastActive: {
          type: DataTypes.DATE,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('Conversations', ['createdBy'], {
          type: 'FOREIGN KEY',
          name: 'Conversations_createdBy_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade'
        });
      })
      .then(() => {
        return query.addIndex('Conversations', { fields: ['name'], unique: true });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Conversations');
  }
};
