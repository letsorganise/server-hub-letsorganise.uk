import { hashPwd } from '../../../src/helpers/crypto';
require('dotenv').config({ silent: true });

module.exports = {
  up: async function(query, DataTypes) {
    const hashed = await hashPwd(process.env.DEFAULT_USER_PASSWORD);
    return query
      .bulkInsert('Users', [
        {
          id: '@root',
          createdBy: '@root',
          firstName: 'Root',
          lastName: 'Root',
          email: 'harry@letsorganise.uk',
          password: hashed,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ])
      .then(() => {
        return query.bulkInsert('UserSettings', [
          {
            id: 'da3e4cb7-6397-4bdd-947f-c60eddebb6a5',
            UserId: '@root',
            active: true,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ]);
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.bulkDelete('Users', { id: { $eq: '@root' } }).then(() => {
      return query.bulkDelete('UserSettings', {
        id: { $eq: 'da3e4cb7-6397-4bdd-947f-c60eddebb6a5' }
      });
    });
  }
};
