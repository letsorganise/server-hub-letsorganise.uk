module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Users', {
        id: {
          type: DataTypes.STRING,
          primaryKey: true,
          allowNull: false
        },
        createdBy: {
          type: DataTypes.STRING,
          allowNull: false
        },
        password: {
          type: DataTypes.STRING,
          allowNull: true
        },
        lastActive: {
          type: DataTypes.DATE,
          allowNull: true
        },
        firstName: {
          type: DataTypes.STRING,
          defaultValue: 'Name'
        },
        middleName: {
          type: DataTypes.STRING,
          allowNull: true
        },
        lastName: {
          type: DataTypes.STRING,
          allowNull: true
        },
        email: {
          type: DataTypes.STRING,
          allowNull: true
        },
        phone: {
          type: DataTypes.STRING,
          allowNull: true
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addIndex('Users', { fields: ['id'], unique: true });
      })
      .then(() => {
        return query.addIndex('Users', { fields: ['email'], unique: true });
      });
  },

  down: function(query, DataTypes) {
    return query.dropTable('Users');
  }
};
