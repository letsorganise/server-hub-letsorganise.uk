module.exports = {
  up: function(query, DataTypes) {
    return query
      .createTable('Messages', {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        text: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        sentBy: {
          type: DataTypes.STRING,
          allowNull: false
        },
        ConversationId: {
          type: DataTypes.UUID,
          allowNull: false
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false
        },
        deletedAt: {
          type: DataTypes.DATE,
          allowNull: true
        }
      })
      .then(() => {
        return query.addConstraint('Messages', ['ConversationId'], {
          type: 'FOREIGN KEY',
          name: 'Messages_ConversationId_fkey',
          references: {
            table: 'Conversations',
            field: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        });
      })
      .then(() => {
        return query.addConstraint('Messages', ['sentBy'], {
          type: 'FOREIGN KEY',
          name: 'Messages_sentBy_fkey',
          references: {
            table: 'Users',
            field: 'id'
          },
          onUpdate: 'cascade'
        });
      });
  },

  down: function(query, DataTypes) {
    // return query.dropAllTables();
    return query.dropTable('Messages');
  }
};
