const now = new Date();
export default [
  {
    name: 'Admin',
    id: '4aec553e-5752-4d14-8b44-dfce4d240e87',
    type: 'ADMIN',
    createdAt: now.toISOString(),
    updatedAt: now.toISOString()
  }
];
